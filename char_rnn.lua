local class = require("class")
local optim = require("optim")
local rnn = require("rnn")
local torch = require("torch")


local CharRNN = class("CharRNN")


function CharRNN:__init()
end

function CharRNN:run()
  self:parse_args()
  self:build_lookup()
  self:setup_model()
  self:setup_training()
  self:train()
end

function CharRNN:parse_args()
  local cmd = torch.CmdLine()
  cmd:option("--input_file", "", "Training text file")
  cmd:option("--context_size", 20, "Number of steps for BPTT")
  cmd:option("--batch_size", 100, "Training batch size")
  cmd:option("--layers", 1, "Number of stacked LSTM layers")
  cmd:option("--hidden_units", 512, "Number of hidden units per LSTM layer")
  cmd:option("--learning_rate", 0.001, "Learning rate")
  cmd:option("--summary_interval", 1, "How many training steps per summary output")
  cmd:option("--sample_interval", 10, "How many training step per sample output")
  cmd:option("--sample_size", 20, "How many characters per sample")
  self.args = cmd:parse(arg or {})
end

function CharRNN:build_lookup()
  self.char_to_index = {}
  self.index_to_char = {}

  local file = io.open(self.args.input_file, "rb")
  local raw_data = file:read("*all")
  file:close()
  local next_index = 1
  for i = 1, #raw_data do
    local character = raw_data:sub(i, i)
    if self.char_to_index[character] == nil then
      self.char_to_index[character] = next_index
      self.index_to_char[next_index] = character
      next_index = next_index + 1
    end
  end
  self.vector_size = next_index - 1
  print(string.format("Found %d characters", self.vector_size))
  self.raw_data = raw_data
end

function CharRNN:one_hot_char(character)
  local one_hot = torch.zeros(self.vector_size)
  one_hot[self.char_to_index[character]] = 1
  return one_hot
end

function CharRNN:setup_model()
  self.model = nn.Sequential()
  self.model:add(nn.LSTM(self.vector_size, self.args.hidden_units))
  for i = 2, self.args.layers do
    self.model:add(nn.LSTM(self.args.hidden_units, self.args.hidden_units))
  end
  self.model:add(nn.Linear(self.args.hidden_units, self.vector_size))
  self.model:add(nn.LogSoftMax())

  self.sequencer = nn.Sequencer(self.model)
  print(self.sequencer)

end

function CharRNN:setup_training()
  self.criterion = nn.SequencerCriterion(nn.ClassNLLCriterion())
  self.splitter = nn.SplitTable(2)
  self.current_step = 0
  self.confusion = optim.ConfusionMatrix(self.vector_size)
  self.losses = {}
end

function CharRNN:train()
  while true do
    self:training_step()
    if self.current_step > 1 and self.current_step % self.args.summary_interval == 0 then
      self:run_summary()
    end
  end
end

function CharRNN:training_step()
  local batch = self:read_batch()
  local input_batch, output_batch = self:read_batch()
  local inputs = self.splitter:forward(input_batch)
  local outputs = self.splitter:forward(output_batch)
  self.sequencer:zeroGradParameters()
  local predictions = self.sequencer:forward(inputs)
  local loss = self.criterion:forward(predictions, outputs)

  local grad_outputs = self.criterion:backward(predictions, outputs)
  local grad_inputs = self.sequencer:backward(inputs, grad_outputs)
  self.sequencer:updateParameters(self.args.learning_rate)
  self.current_step = self.current_step + 1

  for index, step_predictions in ipairs(predictions) do
    for batch_index=1,self.args.batch_size do
      self.confusion:add(step_predictions[batch_index], outputs[index][batch_index])
    end
  end
  table.insert(self.losses, loss)
  self.sequencer:forget()
end

function CharRNN:read_batch()
  local input_batch = torch.Tensor(self.args.batch_size, self.args.context_size - 1, self.vector_size)
  local output_batch = torch.Tensor(self.args.batch_size, self.args.context_size - 1)
  for batch_index = 1, self.args.batch_size do
    local example_start_index = math.random(self.raw_data:len() - self.args.context_size + 1)
    local example_end_index = example_start_index + self.args.context_size - 1
    for example_index = example_start_index, example_end_index do
      local context_index = example_index - example_start_index + 1
      local character = self.raw_data:sub(example_index, example_index)
      if context_index == 1 then
        input_batch[batch_index][context_index]:copy(self:one_hot_char(character))
      elseif context_index == self.args.context_size then
        output_batch[batch_index][context_index - 1] = self.char_to_index[character]
      else
        input_batch[batch_index][context_index]:copy(self:one_hot_char(character))
        output_batch[batch_index][context_index - 1] = self.char_to_index[character]
      end
    end
  end
  return input_batch, output_batch
end

function CharRNN:run_summary()
  self.confusion:updateValids()
  local accuracy = self.confusion.totalValid
  local total_loss = 0.0
  local count = 0
  for k, v in pairs(self.losses) do
    total_loss = total_loss + v
    count = count + 1
  end
  local mean_loss = total_loss / count
  print(string.format("%04d - Loss = %5.3f  Acc = %5.3f", self.current_step, mean_loss, accuracy))
  self.losses = {}
  self.confusion:zero()
end


CharRNN():run()
