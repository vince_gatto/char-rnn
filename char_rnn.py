import argparse
import math
import numpy as np
import os
import tensorflow as tf
import time

from tensorflow.models.rnn import rnn
from tensorflow.models.rnn import rnn_cell


class CharRnn(object):
  def __init__(self):
    self.input_file = None

  def Run(self):
    self._ParseArgs()
    self._BuildLookup()
    self._SetupModel()
    self._SetupTraining()
    self._Initialize()
    self._Train()

  def _ParseArgs(self):
    parser = argparse.ArgumentParser()
    parser.add_argument("--input_file", type=str)
    parser.add_argument("--batch_size", type=int, default=100)
    parser.add_argument("--context_size", type=int, default=20)
    parser.add_argument("--layers", type=int, default=1)
    parser.add_argument("--hidden_units", type=int, default=512)
    parser.add_argument("--learning_rate", type=float, default=0.001)
    parser.add_argument("--summary_interval", type=int, default=1)
    parser.add_argument("--sample_interval", type=int, default=10)
    parser.add_argument("--sample_size", type=int, default=20)
    self.args = parser.parse_args()

  def _SetupModel(self):
    # Setup inputs
    self.input_batch = tf.placeholder(
      tf.float32,
      shape=(self.args.batch_size, self.args.context_size, self.vector_size),
      name="input_batch")

    # Split the input into one-character time steps.
    split_input = tf.split(1, self.args.context_size, self.input_batch)
    self.rnn_input = [tf.squeeze(i, squeeze_dims=[1]) for i in split_input]

    # RNN cell factory
    input_cell = rnn_cell.BasicLSTMCell(num_units=self.args.hidden_units, input_size=self.vector_size)
    inner_cell = rnn_cell.BasicLSTMCell(num_units=self.args.hidden_units)
    cells = [input_cell]
    for _ in range(self.args.layers - 1):
      cells.append(inner_cell)
    cell = rnn.rnn_cell.MultiRNNCell(cells)

    self.sample_input = tf.placeholder(tf.float32, shape=(1, self.vector_size))
    self.sample_state = tf.placeholder(tf.float32, shape=(1, cell.state_size))

    with tf.variable_scope("CharRNN") as scope:
      # Setup variables for softmax classifier.
      weights = tf.Variable(tf.random_normal((self.args.hidden_units, self.vector_size)), name="softmax_weights")
      biases = tf.Variable(tf.zeros([self.vector_size]), name="softmax_biases")
      # Initialize the RNN state.
      state = cell.zero_state(self.args.batch_size, tf.float32)
      outputs = []
      self.predictions = []
      # We unroll the RNN, creating a copy of the cell for each timestamp, so we can do BPTT. For
      # each character of the input, we try to predict the following character, so we only unroll
      # for N-1 timesteps.
      for timestep, word_batch in enumerate(self.rnn_input[:-1]):
        if timestep > 0:
          # Now that we've created the variables, set the scope to always try to
          # fetch them instead of creating them.
          scope.reuse_variables()
        output, state = cell(word_batch, state)
        outputs.append(output)
        prediction = tf.nn.softmax(tf.matmul(output, weights) + biases, name="prediction%d" % timestep)
        self.predictions.append(prediction)
      # The unrolled representation is just for BPTT. To periodically generate input based on our
      # learned parameters, we define an extra copy of the cell that just takes on character at a
      # time.
      self.sample_output, self.sample_next_state = cell(self.sample_input, self.sample_state)
      self.sample_prediction = tf.nn.softmax(tf.matmul(self.sample_output, weights) + biases, name="sample_prediction")

  def _SetupTraining(self):
    # At each timestep, we predict the next character and compute the loss based on that prediction.
    self.loss = tf.constant(0.0)
    correctness = []
    for timestep, prediction in enumerate(self.predictions):
      labels = self.rnn_input[timestep+1]
      # TODO(vgatto): Should we do average or total loss?
      self.loss += -1.0 * tf.reduce_mean(tf.reduce_sum(labels * tf.log(prediction + 1e-12), 1))
      predicted_chars = tf.argmax(prediction, 1)
      expected_chars = tf.argmax(labels, 1)
      correctness.append(tf.to_float(tf.equal(predicted_chars, expected_chars)))
    self.accuracy = tf.reduce_mean(tf.concat(0, correctness))
    self.train_step = tf.train.AdamOptimizer(self.args.learning_rate).minimize(self.loss)

  def _Initialize(self):
    self.session = tf.Session()
    self.session.run(tf.initialize_all_variables())

  def _Train(self):
    writer = tf.train.SummaryWriter("/tmp/chr-rnn", self.session.graph_def)
    losses = []
    accuracies = []
    self.current_training_step = 0
    self.start_time = time.time()
    while True:
      self.current_training_step += 1
      example_batch = self._ReadBatch()
      _, loss, accuracy = self.session.run([self.train_step, self.loss, self.accuracy], feed_dict={
        self.input_batch: example_batch,
      })
      losses.append(loss)
      accuracies.append(accuracy)
      if self.current_training_step % self.args.summary_interval == 0:
        mean_loss = sum(losses)/len(losses)
        mean_accuracy = sum(accuracies)/len(accuracies)
        self._Log("loss: %6.4f  accuracy: %5.3f" % (mean_loss, mean_accuracy))
      if self.current_training_step % self.args.sample_interval == 0:
        self._Sample(self.args.sample_size)


  def _Sample(self, count):
    characters = ""
    prediction = [None]
    next_state = np.zeros(self.sample_state.get_shape())

    while len(characters) < count:
      character, next_input = self._SampleInput(prediction[0])
      characters += character
      prediction, next_state = self.session.run([self.sample_prediction, self.sample_next_state], feed_dict={
        self.sample_state: next_state,
        self.sample_input: next_input
      })
    print "Sample --------------------"
    print characters
    print "---------------------------"

  def _SampleInput(self, prediction):
    if prediction is None:
      character = np.random.choice([c for c in "ABCDEFGHIJKLMNOPQRSTUV"])
    else:
      character = self.index_to_char[np.random.choice(self.vector_size, p=prediction)]
    one_hot = np.reshape(self._OneHotChar(character), (1, self.vector_size))
    return (character, one_hot)

  def _OneHotChar(self, character):
    one_hot = [0.0] * self.vector_size
    one_hot[self.char_to_index[character]] = 1.0
    return one_hot

  def _BuildLookup(self):
    self.char_to_index = {}
    self.index_to_char = {}
    for line in open(self.args.input_file):
      for char in line:
        if char in self.char_to_index:
          continue
        self.char_to_index[char] = len(self.char_to_index)
        self.index_to_char[len(self.index_to_char)] = char
    self.vector_size = len(self.char_to_index)

  def _ReadExample(self):
    while True:
      if self.input_file is None:
        self.input_file = open(self.args.input_file)
      read_size = self.args.context_size
      characters = self.input_file.read(read_size)
      if len(characters) != read_size:
        self.input_file = None
      else:
        return [self._OneHotChar(c) for c in characters]

  def _ReadBatch(self):
    batch = []
    for _ in range(self.args.batch_size):
      example = self._ReadExample()
      batch.append(example)
    return np.array(batch)

  def _Log(self, message):
    elapsed = int(time.time() - self.start_time)
    days = elapsed / 86400
    hours = (elapsed % 86400) / 3600
    minutes = ((elapsed % 86400) % 3600) / 60
    seconds = ((elapsed % 86400) % 3600) % 60
    print "%04d %02d:%02d:%02d:%02d - %s" % (
      self.current_training_step,
      days, hours, minutes, seconds,
      message)



if __name__ == "__main__":
  CharRnn().Run()
